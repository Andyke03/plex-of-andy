package be.ordina;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\AnCl\\Documents\\COURSES\\Tools\\Session2\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.plex.tv/");
        WebElement acceptBtn = driver.findElement(By.className("cookie-consent-accept"));
        acceptBtn.click();
        WebElement signupBtn = driver.findElement(By.cssSelector("#plex-site-container > div.grid-x.header.align-middle > div.large-10.cell.menu-header.align-middle > div.header-dynamic.text-right.menu-user.wow.fadeIn > div > div.plexview-navbar-main-user-buttons.dynamic-loading > ul > li:nth-child(2) > a"));
        signupBtn.click();
        Thread.sleep(3000);
        driver.switchTo().frame("fedauth-iFrame");
        WebElement emailAddressInput = driver.findElement(By.id("email"));
        emailAddressInput.sendKeys("moviekes@mailinator.com");
        WebElement password = driver.findElement(By.id("password"));
        password.sendKeys("OrlandoBl666");
        password.submit();
    }
}
