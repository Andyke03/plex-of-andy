package be.ordina.utils;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class SeleniumUtils {
    public static void screenshot(WebDriver driver,String ScreenShotName) throws IOException {
        File screenshot =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("C:\\Users\\AnCl\\Pictures\\Saved Pictures\\" + ScreenShotName + ".png"));
    }
}
