package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

    WebDriver driver;
    WebDriverWait wait;

    private By signupBtn = By.xpath("//a[contains(@class, 'signup button')]");
    private By signinBtn = By.xpath("//*[@id=\"plex-site-container\"]/div[3]/div[2]/div[2]/div/div[1]/ul/li[1]/a");
    private By tbUsername = By.id("email");
    private By tbPassword = By.id("password");
    private By Loginframe = By.id("fedauth-iFrame");
    //private By AcceptBtn = By.xpath("//a[contains(@class,'cookie-consent-accept')]");
    private By launchBtn = By.xpath("//a[contains(@class,'launch') and @data-utm='launch-main-menu']");


    public HomePage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver,10);
    }

    public void clickSignIn() {
        wait.until(ExpectedConditions.elementToBeClickable(signinBtn));
        driver.findElement(signinBtn).click();
        switchToLogin();
    }

    public void switchToLogin(){
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(Loginframe));
    }

    public void LaunchBtn(){
        wait.until(ExpectedConditions.elementToBeClickable(launchBtn));
        driver.findElement(launchBtn).click();
    }

    public void ClickSignUpBtn(){
        wait.until(ExpectedConditions.elementToBeClickable(signupBtn));
        driver.findElement(signupBtn).click();
        switchToLogin();
    }

}
