package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MovieOverview{

    private WebDriver driver;
    private WebDriverWait wait;


    private By movieSection = By.xpath("//div[contains(@class, 'SourceSidebarLink') ] /a[contains(@title, 'Movies & Shows')]");
    private By actionAdventure = By.xpath("//*[@id=\"content\"]/div/div/div[2]/div[3]/div[1]/div/div[1]/div[1]/div/div/div[2]/a");
    private By categoriesTab = By.xpath("//*[@id=\"content\"]/div/div/div[2]/div[1]/div[2]/div[1]/a[3]");
    private By favoriteMovie = By.xpath("//a[contains(@data-qa-id, 'metadataTitleLink')]");
    private By resumeBtn = By.xpath("//button[contains(@data-qa-id,'preplay-play')]");
    private By startBtn = By.xpath("//button[contains(text(),\"Resume from\")]");


    public MovieOverview(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,10);
    }

    public void MoviesShows(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(movieSection));
        driver.findElement(movieSection).click();
    }
    public void Categories(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(categoriesTab));
        driver.findElement(categoriesTab).click();
    }

    public void ActionAdventureClick(){
        //wait.until(ExpectedConditions.visibilityOfElementLocated(actionAdventure));
        wait.until(ExpectedConditions.elementToBeClickable(actionAdventure));
        driver.findElement(actionAdventure).click();

    }
    public void HoverMovieAndPlay(){
        wait.until(ExpectedConditions.presenceOfElementLocated(favoriteMovie));
        driver.findElement(favoriteMovie);
        wait.until(ExpectedConditions.elementToBeClickable(favoriteMovie)).click();

    }
    public void ClickResume(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(resumeBtn));
        driver.findElement(resumeBtn).click();
    }
    public void StartBeginning(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(startBtn));
        driver.findElement(startBtn).click();
    }

    public void MovieToWatch() throws InterruptedException {
        MoviesShows();
        Categories();
        ActionAdventureClick();
        HoverMovieAndPlay();
        ClickResume();
        StartBeginning();
    }
}
