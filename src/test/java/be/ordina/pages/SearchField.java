package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchField {

    private WebDriver driver;
    private WebDriverWait wait;

    private By searchinputfield = By.xpath("//input[@data-qa-id='quickSearchInput']");
    private By magnifiericon = By.id("plex-icon-search");
    private By MovieTitle = By.xpath("//a[contains(@title, 'Britney Spears: Princess of Pop')]");

    public SearchField(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }
    public void SearchInput(String input){
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchinputfield));
        driver.findElement(searchinputfield).sendKeys(input);
    }
    public void GetResultMovie(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(MovieTitle));
        driver.findElement(MovieTitle).getAttribute("title");
        driver.findElement(MovieTitle).click();
    }
    public void LookForSpecificMovie(String input){
        SearchInput(input);
        GetResultMovie();
    }





}









