package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginModal {
    private WebDriver driver;
    private WebDriverWait wait;
    private By signinBtn = By.xpath("//*[@id=\"plex-site-container\"]/div[3]/div[2]/div[2]/div/div[1]/ul/li[1]/a");
    private By tbUsername = By.id("email");
    private By tbPassword = By.id("password");
    private By Loginframe = By.id("fedauth-iFrame");
    private By acceptBtnPath = By.xpath("//a[contains(@class,'cookie-consent-accept')]");
    private By SubmitBtn = By.xpath("//*[@id=\"plex\"]/div/div/div/div[1]/form/button");
    private By Errormessage=By.xpath("//*[@id=\"plex\"]/div/div/div/div[1]/span[1]");
    private By ErrorUsernameMessage=By.xpath("//*[@id=\"plex\"]/div/div/div/div[1]/span[1]");

    public LoginModal (WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver,10);
    }
    public void FillInUsername(String username){
        wait.until(ExpectedConditions.visibilityOfElementLocated(tbUsername));
        driver.findElement(tbUsername).sendKeys(username);
    }
    public void FillInPassword(String password){
        wait.until(ExpectedConditions.visibilityOfElementLocated(tbUsername));
        driver.findElement(tbPassword).sendKeys(password);
    }
    public void ClickSubmitBtn(){
        wait.until(ExpectedConditions.elementToBeClickable(SubmitBtn));
        driver.findElement(SubmitBtn).click();
    }
    public void SignUp(String username, String password){
        FillInUsername(username);
        FillInPassword(password);
        ClickSubmitBtn();
    }
    public String GetErrorMessageText(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(Errormessage));
        return driver.findElement(Errormessage).getText();
    }

    public String GetErrorMessageUsernameText(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(ErrorUsernameMessage));
        return driver.findElement(ErrorUsernameMessage).getText();
    }

    public void switchToLogin(){
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(Loginframe));
    }


    public void SignIn(String username, String password){
        FillInUsername(username);
        FillInPassword(password);
        ClickSubmitBtn();
    }
}
