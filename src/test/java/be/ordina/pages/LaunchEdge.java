package be.ordina.pages;

import be.ordina.utils.SeleniumUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

public class LaunchEdge{

    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void setUp(){
        System.setProperty("webdriver.edge.driver", "C:\\Users\\AnCl\\Documents\\TATP\\drivers\\edgedriver_win32\\MicrosoftWebDriver.exe");
        driver = new EdgeDriver();
        driver.get("https://www.plex.tv/");
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver,10);
    }
    @Test
    public void successfulLogin() throws InterruptedException, IOException {
        HomePage homepage = new HomePage(driver);
        homepage.clickSignIn();
        LoginModal loginModal = new LoginModal(driver);
        loginModal.SignIn("moviesandy@mailinator.com","JohnyDepp256");
        homepage.LaunchBtn();
        Thread.sleep(2500);
        SeleniumUtils.screenshot(driver,"Andy002");
    }

}

