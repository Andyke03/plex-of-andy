package be.ordina;

import be.ordina.pages.SearchField;
import be.ordina.utils.SeleniumUtils;
import org.junit.Assert;

import be.ordina.pages.HomePage;
import be.ordina.pages.LoginModal;
import be.ordina.pages.MovieOverview;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

/**
 * Unit test for simple App.
 */
public class AppTest {

    private WebDriver driver;
    private WebDriverWait wait;

    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        Assert.assertTrue( true );
    }

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\AnCl\\Documents\\TATP\\drivers\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.plex.tv/");
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver,10);

    }
    @After
    public void tearDown() {
        //driver.close();
    }

    @Test
    public void NewUser(){
        HomePage Homepage = new HomePage(driver);
        Homepage.ClickSignUpBtn();
        LoginModal loginModal = new LoginModal(driver);
        loginModal.SignUp("andyplex10@mailinator.com","JessicaChastain069");
    }

    @Test
    public void AccountCreation(){
        HomePage HomePage = new HomePage(driver);
        HomePage.ClickSignUpBtn();
        LoginModal loginModal = new LoginModal(driver);
        loginModal.SignUp("movies@mailinator.com","KevinBacon356");
        Assert.assertEquals("Problem with current username",loginModal.GetErrorMessageUsernameText());
    }

    @Test
    public void Credentialsfail(){
        HomePage homepage = new HomePage(driver);
        homepage.clickSignIn();
        LoginModal loginModal = new LoginModal(driver);
        loginModal.SignIn("gkggl","kdbalkdkkd");

        String message = loginModal.GetErrorMessageText();
        Assert.assertEquals("Wrong inputcombination of username/password",loginModal.GetErrorMessageText());
    }

    @Test
    public void successfulLogin() throws InterruptedException, IOException {
        HomePage homepage = new HomePage(driver);
        homepage.clickSignIn();
        LoginModal loginModal = new LoginModal(driver);
        loginModal.SignIn("moviesandy@mailinator.com","JohnyDepp256");
        homepage.LaunchBtn();
        Thread.sleep(2700);
        SeleniumUtils.screenshot(driver,"Andy002");
    }

    @Test
    public void MovieAndPlay() throws InterruptedException {
        HomePage homepage = new HomePage(driver);
        homepage.clickSignIn();
        LoginModal loginModal = new LoginModal(driver);
        loginModal.SignIn("moviesandy@mailinator.com","JohnyDepp256");
        homepage.LaunchBtn();
        MovieOverview movieOverview = new MovieOverview(driver);
        movieOverview.MovieToWatch();
    }

    @Test
    public void SpecificMovie() throws IOException, InterruptedException {
        HomePage homepage = new HomePage(driver);
        homepage.clickSignIn();
        LoginModal loginModal = new LoginModal(driver);
        loginModal.SignIn("andyplex06@mailinator.com","JessicaChastain069");
        homepage.LaunchBtn();
        SearchField searchField = new SearchField(driver);
        searchField.LookForSpecificMovie("Britney");
        Thread.sleep(1500);
        SeleniumUtils.screenshot(driver, "Britney01");

    }

}

